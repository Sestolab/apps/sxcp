# sxcp

Simple X Color Picker.

## Dependencies

* libxcb

## Installation

* $ make
* # make install

## Examples

Run sxcp, then press any mouse button to print color in hex to standard output:

```
$ sxcp
```

Write output to the clipboard using [sxc](https://sestolab.pp.ua/apps/sxc):

```
$ sxcp | sxc
```

