#include <stdio.h>
#include <stdlib.h>
#include <xcb/xcb.h>


int main(void)
{
	xcb_connection_t *connection = xcb_connect(NULL, NULL);

	if (xcb_connection_has_error(connection))
	{
		fprintf(stderr, "Could not connect to the X server.\n");
		return 1;
	}

	xcb_screen_t *screen = xcb_setup_roots_iterator(xcb_get_setup(connection)).data;
	xcb_grab_pointer_reply_t *grab_pointer = xcb_grab_pointer_reply(connection,
		xcb_grab_pointer(connection, 1, screen->root, XCB_EVENT_MASK_BUTTON_PRESS, XCB_GRAB_MODE_ASYNC,
			XCB_GRAB_MODE_ASYNC, screen->root, XCB_NONE, XCB_CURRENT_TIME), NULL);

	if (grab_pointer->status != XCB_GRAB_STATUS_SUCCESS)
	{
		fprintf(stderr, "Failed to grab pointer.\n");
		free(grab_pointer);
		xcb_disconnect(connection);
		return 1;
	}
	free(grab_pointer);


	xcb_generic_event_t *event = NULL;
	do { event = xcb_wait_for_event(connection); }
	while (event && (event->response_type & ~0x80) != XCB_BUTTON_PRESS);
	free(event);

	xcb_query_pointer_reply_t *pointer = xcb_query_pointer_reply(connection,
		xcb_query_pointer(connection, screen->root), NULL);

	xcb_get_image_reply_t *image = xcb_get_image_reply(connection,
			xcb_get_image(connection, XCB_IMAGE_FORMAT_Z_PIXMAP, screen->root,
			pointer->win_x, pointer->win_y, 1, 1, UINT32_MAX), NULL);

	free(pointer);

	if (xcb_get_image_data_length(image) == 4)
	{
		uint8_t *data = xcb_get_image_data(image);
		printf("%.2x%.2x%.2x", data[2], data[1], data[0]);
	}

	free(image);

	xcb_ungrab_pointer(connection, XCB_CURRENT_TIME);
	xcb_disconnect(connection);
	return 0;
}

