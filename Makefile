CFLAGS += -Wall -Wextra -pedantic -lxcb

PREFIX ?= /usr/local
CC ?= cc

all: sxcp

sxcp: sxcp.c
	$(CC) sxcp.c $(CFLAGS) -o sxcp

install: sxcp
	install -Dm755 sxcp -t $(DESTDIR)$(PREFIX)/bin

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sxcp

clean:
	rm -f sxcp

.PHONY: all install uninstall clean

